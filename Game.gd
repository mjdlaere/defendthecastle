extends Node

const enemy_scene = preload("res://Enemy.tscn")
signal game_over()

const FEE_PER_ENEMY = 100

var round_nr = 0
var units_spawning = randi() % 4 + 5

const wall_price_per_pt = 10

onready var wallet = Wallet.new()

func _ready():
    randomize()
    $Wall.connect("wall_down", self, "emit_game_over")
    $GUI/PurchasingControls/StartNewRoundButton.connect("button_up", self, "start_new_round")
    $GUI/PurchasingControls/RepairWallButton.connect("button_up", self, "repair_wall")
    $GUI/PurchasingControls.hide()
    update_round_label()
    $PlayerSpot.create_player_companion()
    $PlayerSpot/Companion/Player.connect("firearm_changed", self, "register_firearm")
    register_firearm()
    wallet.connect("amount_changed", self, "update_wallet_label")
    $Wall.connect("health_changed", self, "update_wall_label")
    update_wallet_label()
    update_wall_label()
    var companion_spots = get_tree().get_nodes_in_group(GroupNames.COMPANION_SPOTS)
    for c in companion_spots:
        c.wallet = wallet
        c.connect("upkeep_changed", self, "show_upkeep")
    show_upkeep()
    start_new_round()
    
func register_firearm():
    $PlayerSpot/Companion/Player.firearm.connect("ammo_changed", self, "update_ammo_label")
    update_ammo_label()
    $Crosshairs.connect_to_firearm($PlayerSpot/Companion/Player.firearm)

func update_ammo_label():
    var ammo = $PlayerSpot/Companion/Player.firearm.ammo_in_mag
    var capacity = $PlayerSpot/Companion/Player.firearm.mag_capacity
    $GUI/AmmoLabel.text = "Magazine: %s / %s" % [ammo, capacity]

func update_wall_label():
    $GUI/WallLabel.text = "Wall: %s / %s" % [$Wall.health, $Wall.max_health]

func emit_game_over():
    _set_windows_cursor()
    emit_signal("game_over")

func show_upkeep():
    var upkeep = 0
    var companion_spots = get_tree().get_nodes_in_group(GroupNames.COMPANION_SPOTS)
    for c in companion_spots:
        upkeep += c.calculate_upkeep()
    $GUI/UpkeepLabel.text = "Upkeep: $%s" % upkeep

func _set_windows_cursor():
    $Crosshairs.hide()
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
    
func _set_reticle_cursor():
    $Crosshairs.show()
    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _spawn_enemies():
    var min_pos = $SpawnMin.position
    var max_pos = $SpawnMax.position

    var max_bots_per_row = 8
    var rows = 2 + units_spawning / max_bots_per_row

    # create a bot mask for empty spots
    var bot_mask = []
    for i in range(units_spawning):
        bot_mask.append(true)
    var missing_spots = rows * max_bots_per_row - units_spawning
    for i in range(missing_spots):
        bot_mask.append(false)
    bot_mask.shuffle()
    
    var dist_between_rows = 200
    var dist_between_spots = (max_pos.y - min_pos.y) / (max_bots_per_row - 1)
    
    var mask_index = 0
    for row in range(rows):
        for spot in range(max_bots_per_row):
            if bot_mask[mask_index]:
                var x = min_pos.x - row * dist_between_rows
                x -= rand_range(0.0, 0.8 * dist_between_rows)
                var y = min_pos.y + spot * dist_between_spots
                y += rand_range(-0.3 * dist_between_spots, 0.3*dist_between_spots)
                _spawn_enemy(Vector2(x, y))
            mask_index += 1
    units_spawning += 3 + randi() % 8
    
func _spawn_enemy(pos : Vector2):
    var e = enemy_scene.instance()
    e.connect("killed", self, "_on_enemy_down")
    e.position = pos
    add_child(e)
    
func _on_enemy_down():
    wallet.add(FEE_PER_ENEMY)
    var enemies = get_tree().get_nodes_in_group(GroupNames.ENEMIES)
    if enemies.empty():
        _set_windows_cursor()
        $GUI/PurchasingControls.show()
        get_tree().call_group(GroupNames.COMPANIONS, "set_process", false)
        get_tree().call_group(GroupNames.FIREARMS, "stop_firing")
        var companion_spots = get_tree().get_nodes_in_group(GroupNames.COMPANION_SPOTS)
        for c in companion_spots:
            c.charge_upkeep()
            c.start_purchase_mode()
        
func start_new_round():
    _set_reticle_cursor()
    $GUI/PurchasingControls.hide()
    var companion_spots = get_tree().get_nodes_in_group(GroupNames.COMPANION_SPOTS)
    for c in companion_spots:
        c.end_purchase_mode()
        c.reset_weapon()
    get_tree().call_group(GroupNames.COMPANIONS, "set_process", true)
    round_nr += 1
    update_round_label()
    _spawn_enemies()

func update_round_label():
    $GUI/RoundLabel.text = "Round %s" % round_nr

func update_wallet_label():
    $GUI/WalletLabel.text = "Wallet: $%s" % wallet.amount
    
func repair_wall():
    var damage = $Wall.max_health - $Wall.health
    var repaired_damage = min(damage, wallet.amount)
    wallet.subtract(repaired_damage * wall_price_per_pt)
    $Wall.repair(repaired_damage)
    
