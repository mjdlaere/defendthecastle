extends Node

const ENEMIES = "enemies"
const VISIBLE_ENEMIES = "visible_enemies"
const COMPANION_SPOTS = "companion_spots"
const COMPANIONS = "companions"
const FIREARMS = "firearms"
