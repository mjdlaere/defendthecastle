extends Reference
class_name Wallet

signal amount_changed()

var amount = 800 setget set_amount

func set_amount(a):
    amount = a
    emit_signal("amount_changed")

func add(sum):
    set_amount(amount + sum)
    
func subtract(nr):
    set_amount(amount - nr)

