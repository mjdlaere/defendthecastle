extends Node2D

signal upkeep_changed()

const player_scene = preload("res://PlayerController.tscn")
const ai_scene = preload("res://AiController.tscn")
const firearm_scene = preload("res://Firearm.tscn")
const rifle_script = preload("res://Rifle.gd")
const machinegun_script = preload("res://MachineGun.gd")

const COMPANION_PURCHASE_PRICE = 300
const COMPANION_UPKEEP = 500
const MACHINE_GUN_PURCHASE_PRICE = 1000
const MACHINE_GUN_UPKEEP = 300

var wallet : Wallet = null

func reset_weapon():
    if $Companion:
        if $Companion/Player.firearm is MachineGun:
            $Companion/Player.firearm = create_machine_gun()
        elif $Companion/Player.firearm is Rifle:
            $Companion/Player.firearm = create_rifle()

func calculate_upkeep():
    var upkeep = 0
    if $Companion:
        upkeep += COMPANION_UPKEEP
        if $Companion/Player.firearm is MachineGun:
            upkeep += MACHINE_GUN_UPKEEP
    return upkeep
    
# Called when the node enters the scene tree for the first time.
func _ready():
    $HireCompanionButton.connect("button_up", self, "hire_companion")
    $HireCompanionButton.text = "Hire companion ($%s purchase + $%s per round)" % [COMPANION_PURCHASE_PRICE, COMPANION_UPKEEP]
    $BuyMachineGunButton.text = "Buy machine gun ($%s purchase + $%s per round)" % [MACHINE_GUN_PURCHASE_PRICE, MACHINE_GUN_UPKEEP]
    $BuyMachineGunButton.connect("button_up", self, "buy_machine_gun")
    $BuyMachineGunButton.disabled = true
    
func _draw():
    if $HireCompanionButton.visible && !$Companion:
        var width = 5
        draw_line($RectPt1.position, $RectPt2.position, Color.white, width)
        draw_line($RectPt2.position, $RectPt3.position, Color.white, width)
        draw_line($RectPt3.position, $RectPt4.position, Color.white, width)
        draw_line($RectPt4.position, $RectPt1.position, Color.white, width)
    
func create_player_companion():
    var player = player_scene.instance()
    player.name = "Companion"
    add_child(player)
    $HireCompanionButton.disabled = true
    $BuyMachineGunButton.disabled = false
    player.player.firearm = create_rifle()
    emit_signal("upkeep_changed")

func hire_companion():
    if $Companion:
        $Companion.queue_free()
        $HireCompanionButton.text = "Hire companion ($%s purchase + $%s per round)" % [COMPANION_PURCHASE_PRICE, COMPANION_UPKEEP]
        emit_signal("upkeep_changed")
        $BuyMachineGunButton.disabled = true
    elif wallet.amount >= COMPANION_PURCHASE_PRICE:
        var companion = ai_scene.instance()
        companion.name = "Companion"
        add_child(companion)
        companion.player.firearm = create_rifle()
        wallet.subtract(COMPANION_PURCHASE_PRICE)
        $HireCompanionButton.text = "Fire companion (save $%s per round)" % COMPANION_UPKEEP
        emit_signal("upkeep_changed")
        $BuyMachineGunButton.disabled = false
        
func start_purchase_mode():
    $HireCompanionButton.show()
    $BuyMachineGunButton.show()
    update()

func end_purchase_mode():
    $HireCompanionButton.hide()
    $BuyMachineGunButton.hide()
    update()

func buy_machine_gun():
    if wallet.amount >= MACHINE_GUN_PURCHASE_PRICE:
        var companion = $Companion
        companion.player.firearm = create_machine_gun()
        wallet.subtract(MACHINE_GUN_PURCHASE_PRICE)
        $BuyMachineGunButton.disabled = true
        emit_signal("upkeep_changed")

func charge_upkeep():
    wallet.subtract(calculate_upkeep())    

func create_machine_gun():
    var mg = firearm_scene.instance()
    mg.script = machinegun_script
    return mg
    
func create_rifle():
    var rifle = firearm_scene.instance()
    rifle.script = rifle_script
    return rifle
