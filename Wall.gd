extends StaticBody2D

signal wall_down()
signal health_changed()

const max_health = 100
var health = max_health setget set_health

func set_health(h):
    health = h
    emit_signal("health_changed")
    
func damage_wall():
    set_health(health - 9.0)
    $WallHitSound.play()
    if health < 0.0:
        emit_signal("wall_down")
        
func repair(pts):
    var new_health = max(max_health, health + pts)
    set_health(new_health)
