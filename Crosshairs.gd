extends Sprite

const pulling_bullet_modulate_color = Color("631a1a")
const reloading_modulate_color = Color("110000")
const ready_to_fire_modulate_color = Color.white

func connect_to_firearm(fa : Firearm):
    fa.connect("ready_to_fire", self, "_set_ready_to_fire")
    fa.connect("pulling_bullet", self, "_set_pulling_bullet")
    fa.connect("reloading", self, "_set_reloading")
    _set_ready_to_fire()

func _set_ready_to_fire():
    modulate = ready_to_fire_modulate_color

func _set_pulling_bullet():
    modulate = pulling_bullet_modulate_color

func _set_reloading():
    modulate = reloading_modulate_color
    
func _process(delta):
    position = get_global_mouse_position()
