extends Node2D

var _target : KinematicBody2D = null
onready var player = $Player

var _phase = Phase.Idle

var _last_target_position = null
var _target_speed = 0.0

enum Phase {
    Idle,
    Aiming,
    Shooting
   }

func _ready():
    $AimTimer.connect("timeout", self, "_shoot")
    $ReleaseTriggerTimer.connect("timeout", self, "_release_trigger")
    $BetweenShotsTimer.connect("timeout", self, "_shoot")
    $TargetSpeedTimer.connect("timeout", self, "_track_target")
    player.connect("firearm_changed", self, "connect_firearm")
    
func connect_firearm():
    player.firearm.connect("reload_finished", self, "_shoot")

func _process(delta : float):
    if _target:
        player.look_at(_target.position)

        if _phase == Phase.Idle:
            _phase = Phase.Aiming
            var aim_time = rand_range(1.5, 2.0)
            $AimTimer.wait_time = aim_time
            $AimTimer.start()
    else:
        _find_target()

func _track_target():
    if !_target:
        return
        
    if _last_target_position:
        var dt = $TargetSpeedTimer.wait_time
        var dpos = _target.global_position.distance_to(_last_target_position)
        _target_speed = dpos / dt
        
    _last_target_position = _target.global_position

func _shoot():
    if _target:
        _phase = Phase.Shooting
        _pull_trigger()
    
func _pull_trigger():
    var firearm = player.get_firearm()
    firearm.aiming_point = _calculate_aiming_point()
    # add some randomness
    firearm.aiming_point += Vector2(rand_range(0.95, 1.05), rand_range(0.95, 1.05))
    firearm.fire()
    
    if firearm.single_shot:
        $ReleaseTriggerTimer.wait_time = 0.1
    else:       
        var nr_of_bullets = randi() % 4 # fire 2 to 5 bullets in a burst (1 is added anyway since bullet is fired at the same time that the timer fires)
        $ReleaseTriggerTimer.wait_time = nr_of_bullets * firearm.fire_time_per_shot
    $ReleaseTriggerTimer.start()
        
func _release_trigger():
    var firearm = player.get_firearm()
    firearm.stop_firing()
    if firearm.is_empty():
        firearm.reload()
        return
        
    if firearm.single_shot:
        $BetweenShotsTimer.wait_time = 1.0 + firearm.reload_time
    else:       
        $BetweenShotsTimer.wait_time = 3.0
    $BetweenShotsTimer.start()
       
func _find_target():    
    var enemies = get_tree().get_nodes_in_group(GroupNames.VISIBLE_ENEMIES)
    if enemies.size() == 0:
        return
    # pick the closest target
    var min_dist = enemies[0].global_position.distance_squared_to(player.global_position)
    var min_dist_enemy = enemies[0]
    for e in enemies:
        var dist = e.global_position.distance_squared_to(player.global_position)
        if dist < min_dist:
            min_dist = dist
            min_dist_enemy = e
    _target = min_dist_enemy
    _target.connect("killed", self, "_reset_target")

func _reset_target():
    _target = null
    _last_target_position = null
    _target_speed = 0.0
    _phase = Phase.Idle

# Numerical algorithm to find an aiming point in front of the moving target
# by choosing points S on the targeth path and minimizing |t_T - t_B|
# where t_T is the time the Target needs to get to S
# and t_B the time the Bullet needs to get to S
func _calculate_aiming_point():
    var epsilon = 0.1 # time_diff_treshold

    if _target_speed < epsilon:
        return _target.global_position
    
    var s_min = _target.global_position
    var s_max = Vector2(Globals.wall_left_x, s_min.y)
    # take the point in the middle
    var s_50 = s_min + 0.5 * (s_max - s_min)
    
    var iterations = 0
    var max_iterations = 10 # prevent infinite loops or slowly converging problems
    
    while iterations < max_iterations && _eval_target_path_point(s_50) > epsilon:
        iterations += 1
        # take the points at 1/4 and 3/4:
        var s_25 = s_min + 0.25 * (s_max - s_min)
        var s_75 = s_min + 0.75 * (s_max - s_min)
        
        var eps_s_25 = _eval_target_path_point(s_25)
        var eps_s_75 = _eval_target_path_point(s_75)
        
        if eps_s_25 < eps_s_75:
            s_max = s_50
            s_50 = s_25
        else:
            s_min = s_50
            s_50 = s_75
    return s_50
    
# calculates the current evaluation value on the target path for the numerical algorithm in _calculate_aiming_point
func _eval_target_path_point(possible_intersection_point : Vector2):
    var target_pos = _target.global_position
    var shooter_pos = global_position
    var bullet_speed = player.get_firearm().bullet_speed
    var t_T = target_pos.distance_to(possible_intersection_point) / _target_speed
    var t_B = shooter_pos.distance_to(possible_intersection_point) / bullet_speed
    return abs(t_T - t_B)
