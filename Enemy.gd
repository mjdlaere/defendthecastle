extends KinematicBody2D
class_name Enemy

signal killed()

var wall = null
const velocity = Vector2(60.0, 0.0)

var health = 100.0

func _ready():
    visible = false
    $VisibilityNotifier2D.connect("screen_entered", self, "set_visible", [true])
    $VisibilityNotifier2D.connect("screen_entered", self, "add_to_group", [GroupNames.VISIBLE_ENEMIES])
    $AttackTimer.connect("timeout", self, "damage_wall")
    var nr_of_frames = $AnimatedSprite.frames.get_frame_count("walk")
    var frame_offset = randi() % nr_of_frames
    $AnimatedSprite.play()
    $AnimatedSprite.frame = frame_offset
    
func _physics_process(delta):
    var collision = move_and_collide(velocity * delta)
    if collision:
        var obj = collision.collider
        if obj.has_method("damage_wall"):
            wall = obj
            if $AttackTimer.is_stopped():
                $AttackTimer.start()
        else:
            wall = null
            $AttackTimer.stop()

func damage_wall():
    if wall:
        wall.damage_wall()

func take_bullet_damage(damage):
    if !visible:
        return
    health -= damage
    if health < 0.0:
        $AttackTimer.stop()
        $CollisionShape2D.disabled = true
        remove_from_group(GroupNames.ENEMIES)
        remove_from_group(GroupNames.VISIBLE_ENEMIES)
        hide()
        emit_signal("killed")
        $DieSound.play()
        yield($DieSound, "finished")
        queue_free()
    
