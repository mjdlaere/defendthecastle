extends Node

const game_scene = preload("res://Game.tscn")

func _ready():
    var screen_size = OS.get_screen_size()
    var window_size = OS.get_window_size()
    OS.set_window_position(screen_size*0.5 - window_size*0.5)
    
    $GUI/RestartButton.connect("button_up", self, "restart_game")
    $GUI/CreditsButton.connect("button_up", $GUI/CreditsDialog, "show")
    connect_game()
    
    # set variable that is used by AI companions for aiming calculations
    Globals.wall_left_x = $Game/Wall/WallLeftPos.global_position.x
    
func _process(delta):
    if Input.is_action_just_pressed("left_click"):
        $GUI/InstructionsLabel.hide()
    
func restart_game():
    $GUI/GameOverLabel.hide()
    $GUI/RestartButton.hide()
    $GUI/CreditsButton.hide()
    get_tree().paused = false
    var old_game = $Game
    old_game.name = "old_game"
    old_game.queue_free()
    
    var new_game = game_scene.instance()
    new_game.name = "Game"
    add_child(new_game)
    connect_game()

func connect_game():
    $Game.connect("game_over", self, "game_over")
    $Game.connect("game_over", $GUI/InstructionsLabel, "hide")
    BulletFactory.game_node = $Game
    
func game_over():
    get_tree().paused = true
    $GUI/GameOverLabel.show()
    $GUI/RestartButton.show()
    $GUI/CreditsButton.show()
    
