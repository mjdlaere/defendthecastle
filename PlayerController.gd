extends Node2D

onready var player = $Player

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

func _process(delta):
    var mouse_pos = get_global_mouse_position()
    $Player.look_at(mouse_pos)
    $Player.aim_at(mouse_pos)
    
    if Input.is_action_just_pressed("left_click"):
        $Player.firearm.fire()
    if Input.is_action_just_released("left_click"):
        $Player.firearm.stop_firing()
    if Input.is_action_just_released("reload"):
        $Player.firearm.reload()
