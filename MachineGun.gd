extends Firearm
class_name MachineGun

func _ready():
    bullet_speed = 800
    bullet_damage = 60
    mag_capacity = 30
    ammo_in_mag = mag_capacity
    reload_time = 5.0
    fire_time_per_shot = 0.1
    single_shot = false


func get_type_string():
    return "mg"
