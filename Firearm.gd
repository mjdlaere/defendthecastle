extends Node2D
class_name Firearm

signal ammo_changed()
signal bullet_fired()
signal ready_to_fire()
signal pulling_bullet()
signal reloading()
signal reload_finished()
    
export var mag_capacity = 5.0
export var ammo_in_mag : float = mag_capacity setget set_ammo_in_mag

export var single_shot = true
export var fire_time_per_shot = 0.8
export var reload_time = 3.0

var bullet_scene = preload("res://Bullet.tscn")
var bullet_spawn_node : Node2D = null
var bullet_speed = 500
var bullet_damage = 120

var aiming_point = Vector2()
var _trigger_pulled = false

onready var _fire_timer = $FireRateTimer
onready var _reload_timer = $ReloadTimer

func is_empty():
    return ammo_in_mag <= 0

func fire():
    if _fire_timer.is_stopped() && _reload_timer.is_stopped():
        _trigger_pulled = true
        _fire_bullet()
    
func stop_firing():
    _trigger_pulled = false
    
func reload():
    if _reload_timer.is_stopped():
        _reload_timer.start()
        _fire_timer.stop()
        emit_signal("reloading")
    
func set_ammo_in_mag(amount):
    ammo_in_mag = amount
    emit_signal("ammo_changed")
    
# Called when the node enters the scene tree for the first time.
func _ready():

    _fire_timer.connect("timeout", self, "_on_fire_rate_time_passed")
    _reload_timer.connect("timeout", self, "_on_reload_finished")
    call_deferred("_init_timers")
    
# set timers with intervals from child classes (child _ready and variables are initialized after parent _ready)
func _init_timers():
    _fire_timer.wait_time = fire_time_per_shot
    _reload_timer.wait_time = reload_time

func _process(delta):
    pass 
    
func _on_reload_finished():
    set_ammo_in_mag(mag_capacity)
    emit_signal("ready_to_fire")
    emit_signal("reload_finished")
    
func _on_fire_rate_time_passed():
    if !is_empty():
        emit_signal("ready_to_fire")
    if _trigger_pulled && !single_shot:
        _fire_bullet()
    
func _fire_bullet():
    if ammo_in_mag > 0:
        set_ammo_in_mag(ammo_in_mag - 1)
        _fire_timer.start()
        
        var direction = (aiming_point - bullet_spawn_node.global_position).normalized()
        var bullet = bullet_scene.instance()
        bullet.linear_velocity = direction * bullet_speed
        bullet.global_transform = bullet_spawn_node.global_transform
        bullet.damage = bullet_damage
        BulletFactory.add_bullet(bullet)
        emit_signal("bullet_fired")
        if single_shot:
            emit_signal("pulling_bullet")
    
