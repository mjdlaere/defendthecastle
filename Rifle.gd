extends Firearm
class_name Rifle

func _ready():
    bullet_speed = 500
    bullet_damage = 120
    mag_capacity = 5
    ammo_in_mag = mag_capacity
    reload_time = 3.0
    fire_time_per_shot = 2.0
    single_shot = true
    
func get_type_string():
    return "rifle"
