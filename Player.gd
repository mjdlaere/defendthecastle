extends KinematicBody2D

signal firearm_changed()

var firearm : Firearm = null setget set_firearm, get_firearm
const rifle_sound = preload("res://assets/audio/rifle.wav")
const mg_sound = preload("res://assets/audio/mg.wav")


func aim_at(pos : Vector2):
    firearm.aiming_point = pos

func set_firearm(f):
    if firearm:
        firearm.queue_free()
    firearm = f
    f.bullet_spawn_node = $BulletSpawnPos
    add_child(f)
    emit_signal("firearm_changed")
    firearm.connect("bullet_fired", self, "_show_muzzle_flash")
    firearm.connect("ready_to_fire", self, "_play_aim_animation")
    firearm.connect("pulling_bullet", self, "_play_pull_bullet_animation")
    firearm.connect("reloading", self, "_play_reload_animation")
    _play_aim_animation()

func get_firearm():
    return firearm

func _show_muzzle_flash():
    $MuzzleFlash.visible = true
    $MuzzleFlash/Timer.start()
    if firearm is Rifle:
        $GunSound.stream = rifle_sound
    else:
        $GunSound.stream = mg_sound
    $GunSound.play()

func _ready():
    $MuzzleFlash.visible = false
    $MuzzleFlash/Timer.connect("timeout", $MuzzleFlash, "set_visible", [false])

func _play_aim_animation():
    $AnimatedSprite.animation = "%s_aim" % firearm.get_type_string()
    
func _play_pull_bullet_animation():
    $AnimatedSprite.animation = "%s_reload" % firearm.get_type_string()
    $AnimatedSprite.frames.set_animation_loop($AnimatedSprite.animation, false)
    $AnimatedSprite.play()

func _play_reload_animation():
    $AnimatedSprite.animation = "%s_reload" % firearm.get_type_string()
    $AnimatedSprite.frames.set_animation_loop($AnimatedSprite.animation, true)
    $AnimatedSprite.play()

