extends RigidBody2D
class_name Bullet

var damage = 120.0

# Called when the node enters the scene tree for the first time.
func _ready():
    $VisibilityNotifier2D.connect("screen_exited", self, "queue_free")
    connect("body_entered", self, "_on_body_detected")

func _on_body_detected(b : Node):
    if b.has_method("take_bullet_damage"):
        b.take_bullet_damage(damage)
        queue_free()
